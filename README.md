# Romeo & Juliet scraper + parsers
A simple scraper with a couple of parsers that will generate multiple JSON representations of the full text of Romeo & Juliet, originally written by William Shakespeare in the late 1500's.
Note that this is a work in progress and the data may not be fully correct. 

**Warning:** Scraping/parsing everything (using the `--all` parameter) will result in 150 scraping requests and over 1500 sentiment-analysis requests. This takes a while and might cost quite a bit of bandwidth!

## Available formats
The following files are located inside the `output` directory. Most files have both an *.old* and a *.new* version, representing the version of the text. 

### lines.json
This file contains all information that is scraped. It's the base for all other JSON generators and it's probably not what you are looking for.
### lines.{new,old}.json
Pretty much the same as `lines.json` but with the *old* and *new* texts seperate.
### lines_only.{new,old}.json
All lines that are said in the full text are located in a single chronological array in these files.
### sentences_only.{new,old}.json
All sentences that occur in the full text, in chronological order. Sentences are defined as parts of a line that end in a period (.), question mark (?), or exclamation mark (!).
### char_count.{new,old}.json
Contains all characters that are in the text including the amount of occurrences.
### word_count.{new,old}.json
Contains all words that are in the text including the amount of occurrences.
### sentiment_analysis.{new,old}.json
Includes [sentiment analysis](https://en.wikipedia.org/wiki/Sentiment_analysis) for each line. This file is formatted in the same way `lines[.{new,old}].json` is, it just includes extra information that is obtained using the [HP IDOL OnDemand](https://www.idolondemand.com/) [Sentiment Analysis API](https://www.idolondemand.com/developer/apis/analyzesentiment).

## Running the scraper/parsers
Prerequisites: [NodeJS v4.x.x](https://nodejs.org/en/).

1. Clone/download this repository,
1. Fire up a terminal and navigate to the directory containing `parser.js`,
1. Run `npm install`,
1. Run `node parser <parameters>`
1. ???
1. Profit! For real, though: all output is saved to `output/*.json`.

### Parameters

`--all`
Execute all parsers.

`--lines-only`
Execute the `generateLinesOnly()` parser.

`--sentences-only`
Execute the `generateSentencesOnly()` parser.

`--char-count`
Execute the `generateCharCount()` parser.

`--word-count`
Execute the `generateWordCount()` parser.

`--sentiment-analysis`
Execute the `generateSentimentAnalysis()` parser. Note that this parser takes quite a lot of time and resources.

`--sentiment-analysis-api-key`
Required if you're executing the `generateSentimentAnalysis()` parser, you can get a free API key (max. 50.000 requests) at [HP IDOL OnDemand](https://www.idolondemand.com/).

## Forking & Pull requests
Feel free to fork this repository, improve on it and create a PR!

## Source
Full text is being scraped from [sparknotes.com](http://nfs.sparknotes.com/romeojuliet).