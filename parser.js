var fs = require('fs'),
    jsdom = require('jsdom'),
    async = require('async'),
    najax = require('najax'),
    extend = require('extend');
var args = process.argv.slice(2);

// Main object
var parser = {
    init: function() {
        var _this = this;
        var pages = [];

        // No arguments
        if ( !args.length ) {
            console.log('No parameters were specified, use either --all, or any of [--lines-only, --sentences-only, --char-count, --word-count, or --sentiment-analysis]');
            return false;
        }

        // Generate page URLs
        for ( var i = 4; i <= 286; i += 2 ) { // R&J = 286
            pages.push('http://nfs.sparknotes.com/romeojuliet/page_' + i + '.html');
        }

        async.series([
            function(callback) {
                var linesJSON = 'output/lines.json';

                if ( fs.existsSync(linesJSON) ) {
                    console.log('File \'%s\' exists already, delete the file to rescrape/reparse.\n', linesJSON);
                    callback(null, JSON.parse(fs.readFileSync(linesJSON, 'utf8')));
                } else {
                    console.log('Scraping...');
                    _this.scrape(pages, function(err, result) {
                        console.log(':) Scraping successful\n\nParsing lines...');

                        _this.parseLines(result, function(err, result) {
                            console.log(':) Parsing successful\n');

                            // Write (cache) 'all' lines so we can use them next time
                            _this.saveJSON(result, 'output/lines.json');

                            callback(null, result);
                        });
                    });
                }
            }
        ], function(err, result) {
            if ( err ) throw err;
            result = result[0];

            _this.saveJSON(result.old, 'output/lines.old.json');
            _this.saveJSON(result.new, 'output/lines.new.json');

            // Functions to parse data into seperate JSON files
            if ( args.indexOf('--all') >= 0 ) {
                _this.generateLinesOnly(result.old, 'old');
                _this.generateLinesOnly(result.new, 'new');
                _this.generateSentencesOnly(result.old, 'old');
                _this.generateSentencesOnly(result.new, 'new');
                _this.generateCharCount(result.old, 'old');
                _this.generateCharCount(result.new, 'new');
                _this.generateWordCount(result.old, 'old');
                _this.generateWordCount(result.new, 'new');
                _this.generateSentimentAnalysis(result.old, 'old');
                _this.generateSentimentAnalysis(result.new, 'new');

                i++;
            } else {
                if ( args.indexOf('--lines-only') >= 0 ) {
                    _this.generateLinesOnly(result.old, 'old');
                    _this.generateLinesOnly(result.new, 'new');

                    i++;
                }
                if ( args.indexOf('--sentences-only') >= 0 ) {
                    _this.generateSentencesOnly(result.old, 'old');
                    _this.generateSentencesOnly(result.new, 'new');

                    i++;
                }
                if ( args.indexOf('--char-count') >= 0 ) {
                    _this.generateCharCount(result.old, 'old');
                    _this.generateCharCount(result.new, 'new');

                    i++;
                }
                if ( args.indexOf('--word-count') >= 0 ) {
                    _this.generateWordCount(result.old, 'old');
                    _this.generateWordCount(result.new, 'new');

                    i++;
                }
                if ( args.indexOf('--sentiment-analysis') >= 0 ) {
                    _this.generateSentimentAnalysis(result.old, 'old');
                    _this.generateSentimentAnalysis(result.new, 'new');

                    i++;
                }
            }
        });
    },
    scrape: function(pages, callback) {
        var functions = [];

        pages.forEach(function(page) {
            functions.push(function(callback) {
                console.log('Scraping %s...', page);

                jsdom.env({
                    url: page,
                    scripts: ['http://code.jquery.com/jquery.js'],
                    done: function(err, window) {
                        var $container = window.$('#noFear-comparison');

                        callback(null, {
                            id: page,
                            contents: $container.html()
                        });
                    }
                });
            });
        });

        async.series(functions, function(err, result) {
            if ( err ) throw err;
            var data = {};

            result.forEach(function(r) {
                data[r.id] = r.contents;
            });

            callback(null, data);
        });
    },
    parseLines: function(pages, callback) {
        var sentences = [];
        var functions = [];

        for ( var page in pages ) {
            (function(page) {
                functions.push(function(callback) {
                    if ( pages.hasOwnProperty(page) ) {
                        var content = pages[page];
                        console.log('Parsing sentences for %s...', page);

                        jsdom.env(
                            content,
                            ['http://code.jquery.com/jquery.js'],
                            function (err, window) {
                                var $ = window.$;
                                var lines = {
                                    old: [],
                                    new: []
                                }

                                // Old
                                $('.noFear-left').each(function() {
                                    var $this = $(this);
                                    if ( !$this.find('div[class*="-line"]').length ) return;

                                    lines.old.push({
                                        character: $this.find('b').text().replace(/\s+/g, ' ').trim(),
                                        line: $this.find('div').text().replace(/([\.\?!])/g, '$1 ').replace(/\s+/g, ' ').trim()
                                    });
                                });

                                // New
                                $('.noFear-right').each(function() {
                                    var $this = $(this);
                                    if ( !$this.find('div[class*="-line"]').length ) return;

                                    lines.new.push({
                                        character: $this.find('b').text().replace(/\./g, '. ').replace(/\s+/g, ' ').trim(),
                                        line: $this.find('div').text().replace(/\./g, '. ').replace(/\s+/g, ' ').trim()
                                    });
                                });

                                callback(null, {
                                    old: lines.old,
                                    new: lines.new
                                });
                            }
                        );
                    }
                });
            })(page);
        }

        async.series(functions, function(err, result) {
            if ( err ) throw err;

            var lines = {
                old: [],
                new: []
            };
            result.forEach(function(r) {
                lines.old = lines.old.concat(r.old);
                lines.new = lines.new.concat(r.new);
            });

            callback(null, lines);
        });
    },
    saveJSON: function(obj, path) {
        fs.writeFileSync(path, JSON.stringify(obj, null, 4), 'utf-8');
        console.log(':) Saved lines to %s', path);
    },

    generateLinesOnly: function(lines, suffix) {
        var result = [];

        lines.forEach(function(line) {
            result.push(line.line);
        });

        this.saveJSON(result, 'output/lines_only.' + suffix + '.json');
    },
    generateSentencesOnly: function(lines, suffix) {
        var result = [];

        lines.forEach(function(line) {
            if ( /\.|!|\?/.test(line.line) ) {
                var sentences = line.line.match(/[^\.!\?]+[\.!\?]+/g).map(Function.prototype.call, String.prototype.trim);
                result = result.concat(sentences);
            } else {
                result.push(line.line);
            }
        });

        this.saveJSON(result, 'output/sentences_only.' + suffix + '.json');
    },
    generateCharCount: function(lines, suffix) {
        var result = {};

        lines.forEach(function(line) {
            var characters = line.line.split('');

            characters.forEach(function(character) {
                if ( !result.hasOwnProperty(character) ) result[character] = 0;
                result[character]++;
            });
        });

        this.saveJSON(this.sortObject(result, 'value', true, true), 'output/char_count.' + suffix + '.json');
    },
    generateWordCount: function(lines, suffix) {
        var result = {};

        lines.forEach(function(line) {
            var words = line.line.split(' ');

            words.forEach(function(word) {
                word = word.toLowerCase().replace(/^[^a-zA-Z0-9\\s]+|[^a-zA-Z0-9\\s]+$/g, '');

                if ( !result.hasOwnProperty(word) ) result[word] = 0;
                result[word]++;
            });
        });

        this.saveJSON(this.sortObject(result, 'value', true, true), 'output/word_count.' + suffix + '.json');
    },
    generateSentimentAnalysis: function(lines, suffix) {
        var _this = this;
        var functions = [];

        // Make sure the API key is provided
        var index = args.indexOf('--sentiment-analysis-api-key');
        if ( index < 0 ) {
            console.log('Argument \'--sentiment-analysis-api-key\' is required for the sentiment analysis. Get one at idolondemand.com.');
            return;
        }
        if ( typeof args[index + 1] === 'undefined' ) {
            console.log('Argument \'--sentiment-analysis-api-key\' has no value. Use \'--sentiment-analysis-api-key <key>\' to insert your API key.');
            return;
        }
        var apiKey = args[index + 1].trim();

        lines.forEach(function(line, index) {
            if ( !line.line ) return;

            (function(line, apiKey, lineCount, index) {
                functions.push(function(callback) {
                    najax('https://api.idolondemand.com/1/api/sync/analyzesentiment/v1?text=' + encodeURIComponent(line.line) + '&apikey=' + apiKey, function(json) {
                        console.log('[%s/%s] Sentiment analysis completed for "%s"', index + 1, lineCount, line.line);
                        callback(null, extend(true, {}, line, JSON.parse(json)));
                    }).error(function(err) {
                        console.log(':( Something went wrong while analyzing sentiment for:\n%s\n\n%s', line.line, JSON.stringify(err, null, 4));
                    });
                });
            })(line, apiKey, lines.length, index);
        });

        console.log('');
        async.series(functions, function(err, result) {
            if ( err ) throw err;

            _this.saveJSON(result, 'output/sentiment_analysis.' + suffix + '.json');
        });
    },
    sortObject: function(obj, type, caseSensitive, reverse) {
        var temp_array = [];

        for (var key in obj) {
            if ( obj.hasOwnProperty(key) ) {
                if ( !caseSensitive ) {
                    key = (key['toLowerCase'] ? key.toLowerCase() : key);
                }

                temp_array.push(key);
            }
        }
        if ( typeof type === 'function' ) {
            temp_array.sort(type);
        } else if ( type === 'value' ) {
            temp_array.sort(function(a, b) {
                var x = obj[a];
                var y = obj[b];
                if ( !caseSensitive ) {
                    x = ( x['toLowerCase'] ? x.toLowerCase() : x );
                    y = ( y['toLowerCase'] ? y.toLowerCase() : y );
                }
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        } else {
            temp_array.sort();
        }

        if ( reverse ) temp_array.reverse();

        var temp_obj = {};
        for ( var i = 0; i < temp_array.length; i++ ) {
            temp_obj[temp_array[i]] = obj[temp_array[i]];
        }

        return temp_obj;
    }
}

// Start parsing
parser.init();