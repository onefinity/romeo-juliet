Math.TAU = Math.PI * 2;
String.prototype.ucfirst = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

var colors = {
    old: '#EF5934',
    new: '#2B8BC8',
}

var rj = {
    data: {},
    vars: {
        h: 0,  // Height of the svg element
        w: 0,  // Width of the svg element
        cx: 0, // Horizontal center
        cy: 0, // Vertical center
        colors: {
            old: new Rune.Color(colors.old),
            old_alt: new Rune.Color(colors.old).darken(0.1),
            new: new Rune.Color(colors.new),
            new_alt: new Rune.Color(colors.new).darken(0.1),
            gray: {
                text: new Rune.Color('#ccc'),
                lines: new Rune.Color('#eee')
            }
        }
    },

    init: function() {
        // Retrieve data
        var _this = this;
        var promises = [];
        var files = ['char_count.new', 'char_count.old', 'word_count.new', 'word_count.old', 'sentences_only.new', 'sentences_only.old'];
        files.forEach(function(file) {
            promises.push(_this.getData(file));
        });

        $.when.apply($, promises).then(function() {
            // Extend _this.data with new data
            $.extend(true, _this.data, _.object(files, arguments));

            // Remove loading class
            $('body').removeClass('is-loading');

            // Create Rune
            _this.create();

            // Notify user via console
            console.debug('Use `rj.draw(method[, reset = true])` where method is any of', _this.getMethods());
            console.debug('When you\'re finished, use `rj.save()` to save to .svg or `rj.reset()` to reset canvas');

            // Parse URL paremeters and execute functions
            var search = location.search.substring(1);
            if ( search ) {
                try {
                    var parameters = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
                } catch(e) {
                    console.error('Your URL parameters are invalid.');
                }

                if ( parameters ) {
                    _this.vars.parameters = parameters;

                    if ( parameters.m || parameters.method ) {
                        var method = parameters.m || parameters.method;
                        _this.draw(method);
                    }
                    if ( parameters.s || parameters.save ) {
                        _this.save();
                    }
                }
            }
        }, function(err) {
            console.error(err);
        });
    },
    reset: function() {
        $('svg').remove();
        return this.create();
    },
    create: function() {
        this.r = new Rune({
            container: 'body',
            width: window.innerHeight - 100,
            height: window.innerHeight - 100
        });

        // Update variables
        $.extend(this.vars, {
            w: this.r.width,
            h: this.r.height,
            cx: Math.round(this.r.width/2),
            cy: Math.round(this.r.height/2)
        });

        return this.r;
    },
    save: function() {
        var svg = $(this.r.getEl()).prop('outerHTML');
        console.log('Copy the following code and save it to an .svg file manually:\n\n' + svg);
    },

    draw: function(method, reset) {
        reset = typeof reset !== 'undefined' ? reset : true;
        if ( !method ) {
            console.error('Method was not provided, use any of', this.getMethods());
            return false;
        }
        if ( $.inArray(method, this.getMethods()) < 0 ) {
            console.error('Method \'%s\' does not exist, use any of', method, this.getMethods());
            return false;
        }

        if ( reset ) this.reset();
        this.methods[method](this.r, this.vars, this.data);
        this.r.draw();
    },
    methods: {
        alphabetCircular: function(rune, vars, data) {
            // Options/defaults
            var showCapitals = vars.parameters.capitals,
                oddEven      = stringToBoolean(vars.parameters.oddeven),
                seperators   = stringToBoolean(vars.parameters.seperators);

            // Inner and outer radius
            var ri = vars.w/3.50, // Inner radius
                ro = vars.w/2; // Outer radius

            // Analyze data
            var d = rj.analyzers.alphabet(data);

            var total_min = Math.min(d.old.min.total, d.new.min.total);
            var total_max = Math.max(d.old.max.total, d.new.max.total);

            var g = rune.group(0, 0),
                group_old = rune.group(0, 0, g),
                group_new = rune.group(0, 0, g),
                group_chars = rune.group(0, 0, g),
                group_old_chars = rune.group(0, 0, g),
                group_new_chars = rune.group(0, 0, g);

            // Old
            Object.keys(d.old.chars).sort().forEach(function(char, i) {
                i = ( oddEven ) ? i * 2 : Math.abs(26 - i) - 1;

                var size = range(d.old.chars[char].total, total_min, total_max, ri + 50, ro);
                var x = ( oddEven ) ? -(Math.PI/2) : Math.PI/2;
                if ( showCapitals === 'end' ) {
                    rune.path(vars.cx, vars.cy, group_old)
                        .stroke(false)
                        .fill(vars.colors.old_alt)
                        .lineTo(
                            Math.cos(((i/26) * Math.PI) + x) * size,
                            Math.sin(((i/26) * Math.PI) + x) * size
                        )
                        .lineTo(
                            Math.cos((((i + 1)/26) * Math.PI) + x) * size,
                            Math.sin((((i + 1)/26) * Math.PI) + x) * size
                        )
                        .closePath();

                    var size = range(d.old.chars[char].lowercase, total_min, total_max, ri + 50, ro);
                    rune.path(vars.cx, vars.cy, group_old)
                        .stroke(false)
                        .fill(vars.colors.old)
                        .lineTo(
                            Math.cos(((i/26) * Math.PI) + x) * size,
                            Math.sin(((i/26) * Math.PI) + x) * size
                        )
                        .lineTo(
                            Math.cos((((i + 1)/26) * Math.PI) + x) * size,
                            Math.sin((((i + 1)/26) * Math.PI) + x) * size
                        )
                        .closePath();
                } else {
                    rune.path(vars.cx, vars.cy, group_old)
                        .stroke(false)
                        .fill(vars.colors.old)
                        .lineTo(
                            Math.cos(((i/26) * Math.PI) + x) * size,
                            Math.sin(((i/26) * Math.PI) + x) * size
                        )
                        .lineTo(
                            Math.cos((((i + 1)/26) * Math.PI) + x) * size,
                            Math.sin((((i + 1)/26) * Math.PI) + x) * size
                        )
                        .closePath();
                }

                if ( showCapitals === 'start' ) {
                    var size = range(d.old.chars[char].uppercase, total_min, total_max, ri, ro);
                    rune.path(vars.cx, vars.cy, group_old)
                        .stroke(vars.colors.old_alt)
                        .fill(vars.colors.old_alt)
                        .lineTo(
                            Math.cos(((i/26) * Math.PI) + (Math.PI/2)) * size,
                            Math.sin(((i/26) * Math.PI) + (Math.PI/2)) * size
                        )
                        .lineTo(
                            Math.cos((((i + 1)/26) * Math.PI) + (Math.PI/2)) * size,
                            Math.sin((((i + 1)/26) * Math.PI) + (Math.PI/2)) * size
                        )
                        .closePath();
                }

                var rotation = ( oddEven ) ? i : i - 26;
                rune.text(formatNumber(d.old.chars[char].total), vars.cx + ri + 10, vars.cy, group_old)
                    .fill(255)
                    .stroke(false)
                    .fontSize(14)
                    .textAlign('left')
                    .fontFamily('Arial')
                    .rotate(range(rotation, 0, 26, -90, 90) + 4.5, vars.cx, vars.cy)
            });

            // New
            Object.keys(d.new.chars).sort().forEach(function(char, i) {
                if ( oddEven ) i = i * 2 + 1;
                var size = range(d.new.chars[char].total, total_min, total_max, ri + 50, ro);
                if ( showCapitals === 'end' ) {
                    rune.path(vars.cx, vars.cy, group_new)
                        .stroke(false)
                        .fill(vars.colors.new_alt)
                        .lineTo(
                            Math.cos(((i/26) * Math.PI) - (Math.PI/2)) * size,
                            Math.sin(((i/26) * Math.PI) - (Math.PI/2)) * size
                        )
                        .lineTo(
                            Math.cos((((i + 1)/26) * Math.PI) - (Math.PI/2)) * size,
                            Math.sin((((i + 1)/26) * Math.PI) - (Math.PI/2)) * size
                        )
                        .closePath();

                    var size = range(d.new.chars[char].lowercase, total_min, total_max, ri + 50, ro);
                    rune.path(vars.cx, vars.cy, group_new)
                        .stroke(false)
                        .fill(vars.colors.new)
                        .lineTo(
                            Math.cos(((i/26) * Math.PI) - (Math.PI/2)) * size,
                            Math.sin(((i/26) * Math.PI) - (Math.PI/2)) * size
                        )
                        .lineTo(
                            Math.cos((((i + 1)/26) * Math.PI) - (Math.PI/2)) * size,
                            Math.sin((((i + 1)/26) * Math.PI) - (Math.PI/2)) * size
                        )
                        .closePath();
                } else {
                    rune.path(vars.cx, vars.cy, group_new)
                        .stroke(false)
                        .fill(vars.colors.new)
                        .lineTo(
                            Math.cos(((i / 26) * Math.PI) - (Math.PI / 2)) * size,
                            Math.sin(((i / 26) * Math.PI) - (Math.PI / 2)) * size
                        )
                        .lineTo(
                            Math.cos((((i + 1) / 26) * Math.PI) - (Math.PI / 2)) * size,
                            Math.sin((((i + 1) / 26) * Math.PI) - (Math.PI / 2)) * size
                        )
                        .closePath();
                }

                if ( showCapitals === 'start' ) {
                    size = range(d.new.chars[char].uppercase, total_min, total_max, ri, ro);
                    rune.path(vars.cx, vars.cy, group_new)
                        .stroke(vars.colors.new_alt)
                        .fill(vars.colors.new_alt)
                        .lineTo(
                            Math.cos(((i/26) * Math.PI) - (Math.PI/2)) * size,
                            Math.sin(((i/26) * Math.PI) - (Math.PI/2)) * size
                        )
                        .lineTo(
                            Math.cos((((i + 1)/26) * Math.PI) - (Math.PI/2)) * size,
                            Math.sin((((i + 1)/26) * Math.PI) - (Math.PI/2)) * size
                        )
                        .closePath();
                }

                rune.text(formatNumber(d.new.chars[char].total), vars.cx + ri + 10, vars.cy, group_new)
                    .fill(255)
                    .stroke(false)
                    .fontSize(14)
                    .textAlign('left')
                    .fontFamily('Arial')
                    .rotate(range(i, 0, 26, -90, 90) + 4.4, vars.cx, vars.cy)
            });

            // Middle circle
            rune.circle(vars.cx, vars.cy, ri, group_chars)
                .fill(255)
                .stroke(false);

            if ( seperators ) {
                var l = Object.keys(d.old.chars).length;
                for ( var i = 0; i < l; i++ ) {
                    var w = 35,
                        x = (vars.w/2 + ri) - w,
                        y = vars.h/2

                    rune.line(x, y, x + w, y)
                        .stroke(vars.colors.gray.lines)
                        .rotate((i * (360/l)) + ((360/l)/2), vars.w/2, vars.h/2)
                }
            }

            if ( oddEven ) {
                Object.keys(d.old.chars).sort().forEach(function(char, i) {
                    i *= 2;
                    rune.text(char, vars.cx + ri - 20, vars.cy, group_chars)
                        .fill(vars.colors.gray.text)
                        .stroke(false)
                        .fontSize(14)
                        .textAlign('center')
                        .fontFamily('Arial')
                        .rotate(range(i, 0, 26, -90, 90) + 8, vars.cx, vars.cy)
                });
            } else {
                // Old on top of middle circle
                Object.keys(d.old.chars).sort().forEach(function(char, i) {
                    i = Math.abs(26 - i) - 1;
                    rune.text(char, vars.cx + ri - 20, vars.cy, group_old_chars)
                        .fill(vars.colors.gray.lines)
                        .stroke(false)
                        .fontSize(14)
                        .textAlign('center')
                        .fontFamily('Arial')
                        .rotate(range(i - 26, 0, 26, -90, 90) + 4.5, vars.cx, vars.cy)
                });

                // New on top of middle circle
                Object.keys(d.new.chars).sort().forEach(function(char, i) {
                    rune.text(char, vars.cx + ri - 20, vars.cy, group_new_chars)
                        .fill(vars.colors.gray.lines)
                        .stroke(false)
                        .fontSize(14)
                        .textAlign('center')
                        .fontFamily('Arial')
                        .rotate(range(i, 0, 26, -90, 90) + 4.25, vars.cx, vars.cy)
                });
            }

            // Hacky solution to flip upside-down numbers rightside-up again
            // Too bad Rune.js isn't this advanced yet :(
            setTimeout(function() {
                var $svg = $('svg');
                $svg.find('text').each(function() {
                    var $this     = $(this),
                        attr      = $this.attr('transform'),
                        transform = attr.match(/rotate\(\s*([^)]+?)\s*\)/)[1].split(' '),
                        rotate    = parseFloat(transform[0]);

                    if ( rotate < -90 || rotate > 90 ) { // Left side
                        $this.attr('transform', '');
                        var x = ($this.offset().left - $svg.offset().left),
                            y = ($this.offset().top - $svg.offset().top);

                        transform[0] = parseFloat(transform[0]);

                        if ( $this.attr('text-anchor') === 'start' ) { // Counts
                            $this.attr('text-anchor', 'end');
                        } else if ( $this.attr('text-anchor') === 'middle' ) { // Characters
                            x += 3.75;
                        }

                        var value = 'rotate(';
                        value += transform.join(' ');
                        value += ') rotate(';
                        value += [180, x, y + 8].join(' ');
                        value += ')'
                        $this.attr('transform', value);
                    }
                });
            }, 0);
        },
        alphabetDiff: function(rune, vars, data) {
            var offset = 30;

            // Analyze data
            var d = rj.analyzers.alphabet(data);

            // Groups
            var g = rune.group(0, 0),
                group_old = rune.group(0, 0, g),
                group_new = rune.group(0, 0, g);

            rune.line(offset, vars.h/2, vars.w - offset, vars.h/2, g)
                .stroke(vars.colors.gray.lines)

            // Calculate min and max diff for range
            var diff_min, diff_max;
            Object.keys(d.old.chars).sort().forEach(function(char) {
                var diff = Math.abs(d.old.chars[char].total - d.new.chars[char].total);
                if ( typeof diff_min === 'undefined' ) diff_min = diff;
                if ( typeof diff_max === 'undefined' ) diff_max = diff;
                if ( diff < diff_min ) diff_min = diff;
                if ( diff > diff_max ) diff_max = diff;
            });

            var width = (vars.w - offset * 2) / 26;
            Object.keys(d.old.chars).sort().forEach(function(char, i) {
                var diff = Math.abs(d.old.chars[char].total - d.new.chars[char].total);
                var rdiff = range(diff, diff_min, diff_max, 0, 200);
                if ( d.old.chars[char].total > d.new.chars[char].total ) {
                    rune.rect((width * i) + offset, (vars.h/2) - rdiff, width, rdiff, group_old)
                        .fill(vars.colors.old)
                        .stroke(false)
                } else if ( d.old.chars[char].total < d.new.chars[char].total ) {
                    rune.rect((width * i) + offset, vars.h/2, width, rdiff, group_new)
                        .fill(vars.colors.new)
                        .stroke(false)
                }
            });
        },
        lineLength: function(rune, vars, data) {
            var type = 'default';
            if ( typeof vars.parameters.type !== 'undefined' ) type = vars.parameters.type;
            if ( typeof vars.parameters.legend !== 'undefined' ) legend = parseInt(vars.parameters.legend);
            if ( typeof vars.parameters.description !== 'undefined' ) description = vars.parameters.description;

            // Groups
            var g = rune.group(0, 0),
                group_legend = rune.group(0, 0, g),
                group_old = rune.group(0, 0, g),
                group_new = rune.group(0, 0, g);

            // Data
            var d = {
                old: {
                    lengths: {}
                },
                new: {
                    lengths: {}
                }
            };

            data['sentences_only.old'].forEach(function(sentence) {
                if ( typeof d.old.lengths[sentence.length] === 'undefined' ) d.old.lengths[sentence.length] = 0;
                d.old.lengths[sentence.length]++;
            });

            data['sentences_only.new'].forEach(function(sentence) {
                if ( typeof d.new.lengths[sentence.length] === 'undefined' ) d.new.lengths[sentence.length] = 0;
                d.new.lengths[sentence.length]++;
            });

            Object.keys(d.old.lengths).forEach(function(key) {
                var value = d.old.lengths[key];
                if ( typeof d.old.min === 'undefined' ) d.old.min = value;
                if ( typeof d.old.max === 'undefined' ) d.old.max = value;
                if ( value < d.old.min ) d.old.min = value;
                if ( value > d.old.max ) d.old.max = value;
            });
            Object.keys(d.new.lengths).forEach(function(key) {
                var value = d.new.lengths[key];
                if ( typeof d.new.min === 'undefined' ) d.new.min = value;
                if ( typeof d.new.max === 'undefined' ) d.new.max = value;
                if ( value < d.new.min ) d.new.min = value;
                if ( value > d.new.max ) d.new.max = value;
            });

            var occ_min = Math.min(d.old.min, d.new.min),
                occ_max = Math.max(d.old.max, d.new.max),
                length_min = 0,
                length_max = Math.max(Math.max.apply(null, Object.keys(d.new.lengths)), Math.max.apply(null, Object.keys(d.old.lengths))),
                min_h = 5,
                max_h = 220,
                offset = 10;

            if ( legend ) {
                for ( var i = 0; i <= length_max; i += legend ) {
                    var x = range(i, length_min, length_max, offset, vars.w - (offset * 2)),
                        y = (vars.h/2) - (max_h/2) - 10, group_legend;

                    rune.rect(Math.floor(x), Math.round(y) - 9, 0.25, (vars.h/2) - y + 9, group_legend)
                        .stroke(vars.colors.gray.lines);
                    rune.text(i, x + 8, y, group_legend)
                        .fill(vars.colors.gray.text)
                        .stroke(false)
                        .fontSize(13)
                        .textAlign('left')
                        .fontFamily('Arial')

                    if ( typeof description !== 'undefined' && i === 0 ) {
                        rune.text(description, x + 8, y - 22, group_legend)
                            .fill(vars.colors.gray.text)
                            .stroke(false)
                            .fontSize(13)
                            .textAlign('left')
                            .fontFamily('Arial')
                    }
                }
            }

            Object.keys(d.old.lengths).forEach(function(key) {
                var value = d.old.lengths[key],
                    h = range(value, occ_min, occ_max, min_h, max_h),
                    x = range(key, length_min, length_max, offset, vars.w - (offset * 2));

                switch ( type ) {
                    case 'default':
                        var y = (vars.h * 0.33) - (h / 2);

                        rune.rect(x, y, 1, h, group_old)
                            .stroke(vars.colors.old, 0.75);

                        break;
                    case 'over':
                        var y = (vars.h / 2) - (h / 2);

                        rune.rect(x, y, 0.1, h, group_old)
                            .stroke(vars.colors.old);

                        break;
                    case 'under':
                        var y = (vars.h / 2) - (h / 2);

                        rune.rect(x, y, 0.5, (h / 2) - 0.5, group_old)
                            .stroke(vars.colors.old, 0.5);

                        break;
                    default:
                        console.error('Unknown type %s', type);
                }
            });

            Object.keys(d.new.lengths).forEach(function(key) {
                var value = d.new.lengths[key];

                switch ( type ) {
                    case 'default':
                        var h = range(value, occ_min, occ_max, min_h, max_h);
                        var x = range(key, length_min, length_max, offset, vars.w - (offset * 2));
                        var y = (vars.h * 0.66) - (h / 2);

                        rune.rect(x, y, 1, h, group_new)
                            .stroke(vars.colors.new, 0.75);

                        break;
                    case 'over':
                        var h = range(value, occ_min, occ_max, min_h, max_h);
                        var x = range(key, length_min, length_max, offset, vars.w - (offset * 2));
                        var y = (vars.h / 2) - (h / 2);

                        rune.rect(x, y, 0.1, h, group_new)
                            .stroke(vars.colors.new, 0.5);

                        break;
                    case 'under':
                        var h = range(value, occ_min, occ_max, min_h, max_h);
                        var x = range(key, length_min, length_max, offset, vars.w - (offset * 2));
                        var y = (vars.h / 2) - (h / 2);

                        rune.rect(x, y + (h / 2) + 0.5, 0.5, h / 2, group_new)
                            .stroke(vars.colors.new, 0.5);

                        break;
                    default:
                        console.error('Unknown type %s', type);
                }
            });
        },
        wordCount: function(rune, vars, data) {
            var offset = 30;
            var words = {
                'grammatical_person': {
                    'Me':  ['i', 'me', 'my'],
                    'You': ['you', 'your', 'thou', 'thy'],
                    'He/she':  ['he', 'she', 'his', 'her']
                },
                // https://en.wikipedia.org/wiki/Most_common_words_in_English
                'most_common': {
                    'the':  ['the'],
                    'be':   ['be'],
                    'to':   ['to'],
                    'of':   ['of'],
                    'and':  ['and'],
                    'a':    ['a'],
                    'in':   ['in'],
                    'that': ['that'],
                    'have': ['have'],
                    'i':    ['i'],
                }
            }

            // Groups
            var g = rune.group(0, 0);
            var groups = []; // Will hold dynamically generated groups

            var d = rj.analyzers.wordCount(data, words);
            var charts = Object.keys(d.old).length;

            var w_min, w_max, i_min, i_max;
            Object.keys(d.old).forEach(function(key) {
                var c = Object.keys(d.old[key]).length;
                if ( typeof w_min === 'undefined' ) w_min = c;
                if ( typeof w_max === 'undefined' ) w_max = c;

                if ( c < w_min ) w_min = c;
                if ( c > w_max ) w_max = c;

                Object.keys(d.old[key]).forEach(function(type) {
                    if ( typeof i_min === 'undefined' ) i_min = d.old[key][type];
                    if ( typeof i_max === 'undefined' ) i_max = d.old[key][type];

                    if ( d.old[key][type] < i_min ) i_min = d.old[key][type];
                    if ( d.new[key][type] < i_min ) i_min = d.new[key][type];
                    if ( d.old[key][type] > i_max ) i_max = d.old[key][type];
                    if ( d.new[key][type] > i_max ) i_max = d.new[key][type];
                });
            });

            Object.keys(d.old).forEach(function(key, i) {
                var spacing = 8;
                var items = Object.keys(d.old[key]).length;

                var w = (vars.w - offset) * (items / w_max);
                var h = (i + 1) * (vars.h/(charts + 1));
                var w_i = ((w - offset) / (items * 2));

                groups[i] = { container: rune.group(0, 0, g) };
                $.extend(true, groups[i], {
                    legend: rune.group(0, 0, groups[i].container),
                    old: rune.group(0, 0, groups[i].container),
                    new: rune.group(0, 0, groups[i].container)
                });

                rune.line(offset + spacing, h, w - spacing, h, groups[i].legend)
                    .stroke(vars.colors.gray.lines)

                // Old
                Object.keys(d.old[key]).forEach(function(type, j) {
                    var h_i = range(d.old[key][type], i_min, i_max, 0, 100);
                    var x = spacing + offset + w_i * (j * 2);

                    rune.rect(x, h - h_i, w_i - (spacing/2), h_i, groups[i].old)
                        .fill(vars.colors.old)
                        .stroke(false);

                    // Count
                    rune.text(d.old[key][type], x + (w_i/2) - 3, h - h_i - 10, groups[i].old)
                        .fill(vars.colors.gray.text)
                        .stroke(false)
                        .fontSize(12)
                        .textAlign('center')
                        .fontFamily('Arial');

                    // Words
                    rune.text(type.ucfirst(), x + w_i - 5, h + 25, groups[i].legend)
                        .fill(vars.colors.gray.text)
                        .stroke(false)
                        .fontSize(13)
                        .textAlign('center')
                        .fontFamily('Arial');
                });

                // New
                Object.keys(d.new[key]).forEach(function(type, j) {
                    var h_i = range(d.new[key][type], i_min, i_max, 0, 100);
                    var x = (spacing / 2) + offset + w_i * ((j * 2) + 1);

                    rune.rect(x, h - h_i, w_i - (spacing/2), h_i, groups[i].new)
                        .fill(vars.colors.new)
                        .stroke(false);

                    // Count
                    rune.text(d.new[key][type], x + (w_i/2) - 3, h - h_i - 10, groups[i].old)
                        .fill(vars.colors.gray.text)
                        .stroke(false)
                        .fontSize(12)
                        .textAlign('center')
                        .fontFamily('Arial');
                });
            })
        }
    },
    getMethods: function() {
        return Object.keys(this.methods);
    },

    analyzers: {
        alphabet: function(data) {
            // Analyze and format data
            var d = {
                old: {
                    chars: {}
                },
                new: {
                    chars: {}
                }
            };

            var o = data['char_count.old'];
            Object.keys(o).forEach(function(char) {
                if ( char.match(/^[a-z]+$/i) ) {
                    var value = o[char];
                    var char_key = char.toUpperCase();

                    if ( typeof d.old.chars[char_key] === 'undefined' ) {
                        d.old.chars[char_key] = {
                            'lowercase': 0,
                            'uppercase': 0,
                            'total': 0
                        }
                    }

                    // Upper/lowercase
                    if ( char === char.toUpperCase() ) {
                        d.old.chars[char_key].uppercase += value;
                    } else {
                        d.old.chars[char_key].lowercase += value;
                    }

                    // Total
                    d.old.chars[char_key].total += value;
                }
            });

            var n = data['char_count.new'];
            Object.keys(n).forEach(function(char) {
                if ( char.match(/^[a-z]+$/i) ) {
                    var value = n[char];
                    var char_key = char.toUpperCase();

                    if ( typeof d.new.chars[char_key] === 'undefined' ) {
                        d.new.chars[char_key] = {
                            'lowercase': 0,
                            'uppercase': 0,
                            'total': 0
                        }
                    }

                    // Upper/lowercase
                    if ( char === char.toUpperCase() ) {
                        d.new.chars[char_key].uppercase += value;
                    } else {
                        d.new.chars[char_key].lowercase += value;
                    }

                    // Total
                    d.new.chars[char_key].total += value;
                }
            });

            // Min/max
            Object.keys(d.old.chars).forEach(function(char) {
                var value = $.extend(d.old.chars[char], {char: char});
                if ( typeof d.old.min === 'undefined' ) d.old.min = value;
                if ( typeof d.old.max === 'undefined' ) d.old.max = value;

                if ( value.total < d.old.min.total ) d.old.min = value;
                if ( value.total > d.old.max.total ) d.old.max = value;
            });

            Object.keys(d.new.chars).forEach(function(char) {
                var value = $.extend(d.new.chars[char], {char: char});
                if ( typeof d.new.min === 'undefined' ) d.new.min = value;
                if ( typeof d.new.max === 'undefined' ) d.new.max = value;

                if ( value.total < d.new.min.total ) d.new.min = value;
                if ( value.total > d.new.max.total ) d.new.max = value;
            });

            return d;
        },
        wordCount: function(data, words) {
            var d = {
                old: {},
                new: {}
            };

            // Loop over words
            Object.keys(words).forEach(function(type) {
                if ( typeof d.old[type] === 'undefined' ) d.old[type] = {};
                if ( typeof d.new[type] === 'undefined' ) d.new[type] = {};

                // Loop over types
                Object.keys(words[type]).forEach(function(value) {
                    if ( typeof d.old[type][value] === 'undefined' ) d.old[type][value] = 0;
                    if ( typeof d.new[type][value] === 'undefined' ) d.new[type][value] = 0;

                    // Loop over values
                    words[type][value].forEach(function(word) {
                        word = word.toLowerCase();
                        var co = data['word_count.old'][word];
                        var cn = data['word_count.new'][word];

                        if ( !isNaN(co) ) d.old[type][value] += co;
                        if ( !isNaN(cn) ) d.new[type][value] += cn;
                    });
                });
            });

            return d;
        }
    },

    getData: function(file) {
        var d = new $.Deferred();

        $.getJSON('../output/' + file + '.json').done(function(result) {
            d.resolve(result);
        }).fail(function(err) {
            d.reject(err);
        });

        return d.promise();
    }
}
rj.init();

// Helpers
function range(old_value, old_min, old_max, new_min, new_max) {
    return ((old_value - old_min) / (old_max - old_min)) * (new_max - new_min) + new_min;
}
function formatNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
function stringToBoolean(string) {
    if ( typeof string === 'undefined' ) return false;
    switch(string.toLowerCase().trim()) {
        case "true": case "yes": case "1": return true;
        case "false": case "no": case "0": case null: return false;
        default: return Boolean(string);
    }
}